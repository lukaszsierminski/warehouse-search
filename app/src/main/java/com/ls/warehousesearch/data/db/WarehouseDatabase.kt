package com.ls.warehousesearch.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ls.warehousesearch.data.dao.HolderDao
import com.ls.warehousesearch.data.dao.ItemTypeDao
import com.ls.warehousesearch.data.dao.ItemTypeHolderDao
import com.ls.warehousesearch.model.Holder
import com.ls.warehousesearch.model.ItemType
import com.ls.warehousesearch.model.ItemTypeHolder

@Database(entities = [Holder::class, ItemType::class, ItemTypeHolder::class], version = 1)
abstract class WarehouseDatabase : RoomDatabase() {
    abstract fun holders(): HolderDao
    abstract fun itemTypes(): ItemTypeDao
    abstract fun itemHolderTypes(): ItemTypeHolderDao
}