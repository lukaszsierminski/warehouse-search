package com.ls.warehousesearch.data

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.RoomDatabase.Callback
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.gson.GsonBuilder
import com.ls.warehousesearch.data.db.WarehouseDatabase
import com.ls.warehousesearch.model.WarehousesDB
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors

class ServiceProvider {

    companion object {
        private const val PEOPLEVOX_TEMP = "http://tmp.peoplevox.net/"

        lateinit var applicationContext: Context

        val peopleVoxRepository = lazy {
            val repository = WarehouseRepository(
                Retrofit.Builder()
                    .baseUrl(PEOPLEVOX_TEMP)
                    .addConverterFactory(GsonConverterFactory.create(createGson()))
                    .build()
                    .create(PeopleVoxService::class.java),
                Room.databaseBuilder(applicationContext, WarehouseDatabase::class.java, "warehouse-db")
                    .addCallback(dbCallback)
                    .build()
            )
            repository
        }

        private val dbCallback: RoomDatabase.Callback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                Executors.newSingleThreadExecutor().execute {
                    val response = peopleVoxRepository.value.fetchWarehousesDB()
                    response.body()?.let {
                        if (response.isSuccessful) {
                            peopleVoxRepository.value.insert(it)
                        }
                    }
                }
            }
        }

        private fun createGson() = GsonBuilder()
            .setPrettyPrinting()
            .registerTypeAdapter(WarehousesDB::class.java, GsonWarehouseDeserializer())
            .create()
    }
}