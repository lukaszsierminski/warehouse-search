package com.ls.warehousesearch.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ls.warehousesearch.model.Item
import com.ls.warehousesearch.model.ItemType

@Dao
interface ItemTypeDao {
    @Query("SELECT * FROM ItemType")
    fun getAll(): LiveData<List<ItemType>>

    @Query("SELECT ItemType.ItemTypeId, Name, Barcode, COUNT(ItemTypeHolder.ItemTypeId) as TotalQuantity FROM ItemType LEFT JOIN ItemTypeHolder ON ItemType.ItemTypeId=ItemTypeHolder.ItemTypeId")
    fun getItemsWithCount(): LiveData<List<Item>>

    @Insert
    fun insertAll(vararg ItemType: ItemType)
}