package com.ls.warehousesearch.data

import com.google.gson.JsonArray
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.ls.warehousesearch.model.*
import java.lang.reflect.Type

class GsonWarehouseDeserializer : JsonDeserializer<WarehousesDB> {

    companion object {
        private const val TABLES_KEY = "t"
        private const val TABLE_NAME_KEY = "n"
        private const val TABLE_ROW_KEY = "r"
        private const val VALUES_KEY = "v"

        private const val HOLDER_TABLE_NAME = "Holder"
        private const val ITEM_TYPE_HOLDER_TABLE_NAME = "ItemTypeHolder"
        private const val ITEM_TYPE_TABLE_NAME = "ItemType"

        private const val HOLDER_ID_INDEX = 0
        private const val HOLDER_BARCODE_INDEX = 2
        private const val HOLDER_NAME_INDEX = 3
        private const val HOLDER_QUANTITY_OF_ITEMS_INDEX = 4

        private const val ITEM_TYPE_ID_INDEX = 0
        private const val ITEM_TYPE_NAME_INDEX = 2
        private const val ITEM_TYPE__BARCODE_INDEX = 4

        private const val ITEM_TYPE_HOLDER_ID_INDEX = 0
        private const val ITH_ITEM_TYPE_ID_INDEX = 1
        private const val ITH_HOLDER_ID_INDEX = 2
        private const val ITH_QUANTITY_ID_INDEX = 1


    }

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext): WarehousesDB {
        val holders = mutableListOf<Holder>()
        val itemTypeHolders = mutableListOf<ItemTypeHolder>()
        val itemTypes = mutableListOf<ItemType>()

        json.asJsonObject.getAsJsonArray(TABLES_KEY)
            .map { jsonElement -> jsonElement.asJsonObject }
            .forEach { table ->
                val tableData = table.getAsJsonArray(TABLE_ROW_KEY)
                when (table.getAsJsonPrimitive(TABLE_NAME_KEY).asString) {
                    HOLDER_TABLE_NAME -> tableData.forEach { row ->
                        val values = row.asJsonObject.getAsJsonArray(VALUES_KEY)
                        holders.add(deserializeHolder(values))
                    }
                    ITEM_TYPE_TABLE_NAME -> tableData.forEach { row ->
                        val values = row.asJsonObject.getAsJsonArray(VALUES_KEY)
                        itemTypes.add(deserializeItemType(values))
                    }
                    ITEM_TYPE_HOLDER_TABLE_NAME -> tableData.forEach { row ->
                        val values = row.asJsonObject.getAsJsonArray(VALUES_KEY)
                        itemTypeHolders.add(deserializeItemTypeHolder(values))
                    }
                }
            }
        return WarehousesDB(holders, itemTypes, itemTypeHolders)
    }

    private fun deserializeHolder(values: JsonArray): Holder {
        return Holder(
            values.get(HOLDER_ID_INDEX).asInt,
            values.get(HOLDER_BARCODE_INDEX).asString,
            values.get(HOLDER_NAME_INDEX).asString,
            values.get(HOLDER_QUANTITY_OF_ITEMS_INDEX).asInt
        )
    }

    private fun deserializeItemType(values: JsonArray): ItemType {
        return ItemType(
            values.get(ITEM_TYPE_ID_INDEX).asInt,
            values.get(ITEM_TYPE_NAME_INDEX).asString,
            values.get(ITEM_TYPE__BARCODE_INDEX).asString
        )
    }

    private fun deserializeItemTypeHolder(values: JsonArray): ItemTypeHolder {
        return ItemTypeHolder(
            values.get(ITEM_TYPE_HOLDER_ID_INDEX).asInt,
            values.get(ITH_ITEM_TYPE_ID_INDEX).asInt,
            values.get(ITH_HOLDER_ID_INDEX).asInt,
            values.get(ITH_QUANTITY_ID_INDEX).asInt
        )
    }
}