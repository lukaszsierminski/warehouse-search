package com.ls.warehousesearch.data

import com.ls.warehousesearch.model.WarehousesDB
import retrofit2.Call
import retrofit2.http.GET

interface PeopleVoxService {
    @GET("test.json")
    fun getWarehousesData(): Call<WarehousesDB>
}