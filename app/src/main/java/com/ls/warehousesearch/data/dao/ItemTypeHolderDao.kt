package com.ls.warehousesearch.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ls.warehousesearch.model.ItemTypeHolder

@Dao
interface ItemTypeHolderDao {

    @Query("SELECT * FROM ItemTypeHolder")
    fun getAll(): LiveData<List<ItemTypeHolder>>

    @Insert
    fun insertAll(vararg ItemTypeHolder: ItemTypeHolder)
}