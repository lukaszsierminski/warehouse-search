package com.ls.warehousesearch.data

import androidx.lifecycle.LiveData
import com.ls.warehousesearch.data.db.WarehouseDatabase
import com.ls.warehousesearch.model.*
import retrofit2.Response

class WarehouseRepository(
    private val peopleVoxService: PeopleVoxService,
    private val peopleVoxDB: WarehouseDatabase
) {

    fun fetchWarehousesDB(): Response<WarehousesDB> {
        return peopleVoxService.getWarehousesData().execute()
    }

    fun getHolders(): LiveData<List<Holder>> = peopleVoxDB.holders().getAll()

    fun getItemTypes(): LiveData<List<ItemType>> = peopleVoxDB.itemTypes().getAll()

    fun getItemsWithCount(): LiveData<List<Item>> = peopleVoxDB.itemTypes().getItemsWithCount()

    fun getLocationsWithItem(itemId: Int): LiveData<List<LocationItemCount>> = peopleVoxDB.holders().getLocationsWithItem(itemId)

    fun insert(warehouses: WarehousesDB) {
        peopleVoxDB.holders().insertAll(*warehouses.holders.toTypedArray())
        peopleVoxDB.itemTypes().insertAll(*warehouses.itemTypes.toTypedArray())
        peopleVoxDB.itemHolderTypes().insertAll(*warehouses.itemTypeHolders.toTypedArray())
    }
}