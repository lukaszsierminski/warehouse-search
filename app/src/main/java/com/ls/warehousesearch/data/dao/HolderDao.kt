package com.ls.warehousesearch.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ls.warehousesearch.model.Holder
import com.ls.warehousesearch.model.LocationItemCount

@Dao
interface HolderDao {
    @Query("SELECT * FROM HOLDER")
    fun getAll() : LiveData<List<Holder>>

//    @Query("SELECT ItemType.ItemTypeId, Name, Barcode, COUNT(ItemTypeHolder.ItemTypeId) as TotalQuantity FROM ItemType LEFT JOIN ItemTypeHolder ON ItemType.ItemTypeId=ItemTypeHolder.ItemTypeId")

    @Query("SELECT Name, COUNT(ItemTypeHolder.ItemTypeId) as Quantity FROM Holder LEFT JOIN ItemTypeHolder ON Holder.HolderId=ItemTypeHolder.HolderId AND ItemTypeHolder.ItemTypeId=:itemId")
    fun getLocationsWithItem(itemId: Int) : LiveData<List<LocationItemCount>>

    @Insert
    fun insertAll(vararg Holder: Holder)
}