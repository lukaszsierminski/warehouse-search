package com.ls.warehousesearch.view.ui

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.ls.warehousesearch.R
import com.ls.warehousesearch.view.adapter.WarehousePagerAdapter
import kotlinx.android.synthetic.main.warehouse_search.*

class MainActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.warehouse_search)
        viewpager.adapter = WarehousePagerAdapter(supportFragmentManager)
    }
}