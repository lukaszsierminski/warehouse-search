package com.ls.warehousesearch.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ls.warehousesearch.R
import com.ls.warehousesearch.model.Holder
import com.ls.warehousesearch.view.LocationViewHolder

class LocationAdapter : RecyclerView.Adapter<LocationViewHolder>() {

    lateinit var holders: List<Holder>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        return LocationViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.warehouse_location_item_row,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = holders.size

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        holder.bind(holders[position])
    }
}