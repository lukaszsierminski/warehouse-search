package com.ls.warehousesearch.view

import androidx.recyclerview.widget.RecyclerView
import com.ls.warehousesearch.databinding.WarehouseProductItemRowBinding
import com.ls.warehousesearch.model.Item

class ProductViewHolder constructor(private val binding: WarehouseProductItemRowBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Item) {
        binding.item = item
        binding.executePendingBindings()
    }
}