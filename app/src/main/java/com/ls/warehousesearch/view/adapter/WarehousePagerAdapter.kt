package com.ls.warehousesearch.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ls.warehousesearch.view.ui.WarehouseTab.*
import com.ls.warehousesearch.view.ui.LocationsFragment
import com.ls.warehousesearch.view.ui.ProductsFragment

class WarehousePagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            LOCATION.ordinal -> LocationsFragment()
            PRODUCTS.ordinal -> ProductsFragment()
            else -> throw RuntimeException("Cannot create ViewPager Fragment at position $position")
        }
    }

    override fun getCount(): Int = values().size

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            LOCATION.ordinal -> LOCATION.name.capitalize()
            PRODUCTS.ordinal -> PRODUCTS.name.capitalize()
            else -> throw RuntimeException("Cannot get page title for position $position")
        }
    }
}