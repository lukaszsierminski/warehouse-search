package com.ls.warehousesearch.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ls.warehousesearch.R
import com.ls.warehousesearch.view.adapter.LocationAdapter
import com.ls.warehousesearch.viewmodel.HolderViewModel
import kotlinx.android.synthetic.main.warehouse_locations.view.*

class LocationsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.warehouse_locations, container, false)
        val recyclerView = view.locationsRecyclerView
        setupRecyclerView(recyclerView)
        initializeLocationAdapter(recyclerView)
        return view
    }

    private fun initializeLocationAdapter(recyclerView: RecyclerView) {
        val locationAdapter = LocationAdapter()
        locationAdapter.holders = emptyList()
        activity?.let { fragmentActivity ->
            val holderViewModel = HolderViewModel(fragmentActivity.application)
            holderViewModel.getHolders().observe(fragmentActivity, Observer {
                locationAdapter.holders = it
                locationAdapter.notifyDataSetChanged()
            })
        }
        recyclerView.adapter = locationAdapter
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        context?.let {
            val itemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
            itemDecoration.setDrawable(it.getDrawable(R.drawable.warehouse_item_divider))
            recyclerView.addItemDecoration(itemDecoration)
        }
    }
}