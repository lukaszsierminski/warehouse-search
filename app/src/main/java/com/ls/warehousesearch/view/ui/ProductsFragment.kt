package com.ls.warehousesearch.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ls.warehousesearch.R
import com.ls.warehousesearch.view.adapter.ProductAdapter
import com.ls.warehousesearch.viewmodel.ProductViewModel
import kotlinx.android.synthetic.main.warehouse_products.view.*

class ProductsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.warehouse_products, container, false)
        val recyclerView = view.productsRecyclerView
        setupRecyclerView(recyclerView)
        initializeProductAdapter(recyclerView)
        return view
    }

    private fun initializeProductAdapter(recyclerView: RecyclerView) {
        val productAdapter = ProductAdapter()
        productAdapter.items = emptyList()
        activity?.let { fragmentActivity ->
            val productViewModel = ProductViewModel(fragmentActivity.application)
            productViewModel.getItemsWithCount().observe(fragmentActivity, Observer {
                productAdapter.items = it
                productAdapter.notifyDataSetChanged()
            })
        }
        recyclerView.adapter = productAdapter
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        context?.let {
            val itemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
            itemDecoration.setDrawable(it.getDrawable(R.drawable.warehouse_item_divider))
            recyclerView.addItemDecoration(itemDecoration)
        }
    }
}