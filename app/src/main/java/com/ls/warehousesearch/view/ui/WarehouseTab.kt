package com.ls.warehousesearch.view.ui

enum class WarehouseTab {
    LOCATION,
    PRODUCTS
}