package com.ls.warehousesearch.view

import androidx.recyclerview.widget.RecyclerView
import com.ls.warehousesearch.databinding.WarehouseLocationItemRowBinding
import com.ls.warehousesearch.model.Holder

class LocationViewHolder constructor(private val binding: WarehouseLocationItemRowBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(location: Holder) {
        binding.location = location
        binding.executePendingBindings()
    }
}