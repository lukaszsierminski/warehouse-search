package com.ls.warehousesearch.view.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ls.warehousesearch.R
import com.ls.warehousesearch.model.Item
import com.ls.warehousesearch.view.ProductViewHolder

class ProductAdapter : RecyclerView.Adapter<ProductViewHolder>() {

    lateinit var items: List<Item>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.warehouse_product_item_row,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        Log.e("DUPA", "BIND")
        holder.bind(items[position])
    }
}