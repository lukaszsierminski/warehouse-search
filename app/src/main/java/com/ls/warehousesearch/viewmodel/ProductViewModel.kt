package com.ls.warehousesearch.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.ls.warehousesearch.data.ServiceProvider
import com.ls.warehousesearch.model.Item
import com.ls.warehousesearch.model.LocationItemCount

class ProductViewModel(app: Application) : AndroidViewModel(app) {

    fun getItemsWithCount(): LiveData<List<Item>> = ServiceProvider.peopleVoxRepository.value.getItemsWithCount()

    fun getLocationsWithItem(itemId: Int): LiveData<List<LocationItemCount>> = ServiceProvider.peopleVoxRepository.value.getLocationsWithItem(itemId)
}