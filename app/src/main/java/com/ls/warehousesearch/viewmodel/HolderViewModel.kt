package com.ls.warehousesearch.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ls.warehousesearch.data.ServiceProvider
import com.ls.warehousesearch.model.Holder

class HolderViewModel(app: Application) : AndroidViewModel(app) {

    fun getHolders(): LiveData<List<Holder>> = ServiceProvider.peopleVoxRepository.value.getHolders()
}