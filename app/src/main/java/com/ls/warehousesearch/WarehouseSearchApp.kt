package com.ls.warehousesearch

import android.app.Application
import android.util.Log
import com.ls.warehousesearch.data.ServiceProvider

class WarehouseSearchApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Log.e("TAG", "OnCreate")
        ServiceProvider.applicationContext = applicationContext
    }
}