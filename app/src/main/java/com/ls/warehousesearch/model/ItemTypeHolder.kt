package com.ls.warehousesearch.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ItemTypeHolder")
data class ItemTypeHolder(@PrimaryKey @ColumnInfo(name = "ItemTypeHolderId") val id: Int,
                          @ColumnInfo(name = "ItemTypeId") val typeId: Int,
                          @ColumnInfo(name = "HolderId") val holderId: Int,
                          @ColumnInfo(name = "Quantity") val quantity: Int)