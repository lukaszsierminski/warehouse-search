package com.ls.warehousesearch.model

import androidx.room.ColumnInfo

data class LocationItemCount(@ColumnInfo(name = "Name") val name: String,
                             @ColumnInfo(name = "Quantity") val quantity: Int
)