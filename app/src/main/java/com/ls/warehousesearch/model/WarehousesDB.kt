package com.ls.warehousesearch.model

data class WarehousesDB(val holders: List<Holder>,
                        val itemTypes: List<ItemType>,
                        val itemTypeHolders: List<ItemTypeHolder>)