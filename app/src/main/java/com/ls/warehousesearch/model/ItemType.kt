package com.ls.warehousesearch.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ItemType")
class ItemType(@PrimaryKey @ColumnInfo(name = "ItemTypeId") val id: Int,
               @ColumnInfo(name = "Name") val name: String,
               @ColumnInfo(name = "Barcode") val barcode: String)