package com.ls.warehousesearch.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Holder")
data class Holder(@PrimaryKey @ColumnInfo(name = "HolderId") val id: Int,
                  @ColumnInfo(name = "Barcode") val barcode: String,
                  @ColumnInfo(name = "Name") val name: String,
                  @ColumnInfo(name = "QuantityOfItems") val quantityOfItems: Int)