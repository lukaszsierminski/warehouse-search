package com.ls.warehousesearch.model

import androidx.room.ColumnInfo

data class Item(@ColumnInfo(name = "ItemTypeId") val id: Int,
                @ColumnInfo(name = "Name") val name: String,
                @ColumnInfo(name = "Barcode") val barcode: String,
                @ColumnInfo(name = "TotalQuantity") val totalQuantity: Int)